from fastapi import APIRouter
import requests

router = APIRouter()

@router.get("/test")
async def testapi():

    response = requests.get("https://golem.company").json()
    return response
